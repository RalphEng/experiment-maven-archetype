@echo ******************
@echo prepare execute archetype
@echo ******************

pushd c:\temp

@echo ******************
@echo create clean execute directory
@echo ******************

rd /S /Q newProject
md newProject
cd newProject

@echo ******************
@echo execute archetype
@echo ******************

call mvn archetype:generate -DarchetypeCatalog=local

@echo ******************
@echo show result
@echo ******************

start explorer c:\temp\newProject
popd