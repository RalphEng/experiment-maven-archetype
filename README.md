experiment-maven-archetype
==========================

Maven Archetype for experiment projects.

Maven can create projects from archetypes.
An archetype can been written by hand or created from an existing project.

If the archetype is created from an existing project, an some customizing for the archtetype creation process (like introducing template variables) is required, then threre are two ways:
- add the modify the generated archetype to SCM (git) and modify the files by hand
- add some post process steps to the Archtetype creation process so that the modification are repeatable

This experiment use the second way.
Because `archetype:create-from-project` generate "only" a good starting point for the archetype, but is not able for further modifications, other tools are used.
That's mainly XSLT to modfiy the genereted XML files and Ant to execute this process.
Ant is chooses, because this is not a normal Maven Build but a scripted process.
Ant is started from within Maven, so no Ant installation is needed.
_More details about this further modifications can been found at the end of this document_  

This example contains 3 batch files to run the example:
- `createArchetype.bat` create an Archetype (de.humanfork.experiment.archetype:maven-archetype-experiment-archetype) from this project and install it locally
- `executeArchetype.bat` can be used the execute the generated Archetype 
- `createAndExecuteArchetype.bat` run both batch files


Documentation of Maven Archetype and maven-archetype-plugin
===========================================================

Archetype structure
-------------------

** Attention **
The Maven-Archtype-Desciptor xml root element is `archetype` since version 2.0.
It was `archetype-descriptor` in older versions (1.0).
Some documentation and guides are not updated, even the maven doumentation is updated (https://issues.apache.org/jira/browse/MNGSITE-292()

- https://www.swtestacademy.com/maven-archetype/
- https://dzone.com/articles/creating-a-custom-maven-archetype
- https://www.baeldung.com/maven-archetype

If the archtype is created by `mvn archetype:create-from-project`, then the maven archetype project root is: `target/generated-sources/archetype`.
- The pom of an archetype has packaging `maven-archetype`.
- It has an forder `src\main\resources\archtype-resources` that contains the archetype template
- It has an file `src\main\resources\META-INF-\maven\archetype-metadata.xml` that contains the archetype desciptor


### Archetype descriptor
https://maven.apache.org/archetype/maven-archetype-plugin/specification/archetype-metadata.html

That file contains `fileSet` to define how to use the template files in project creation
- `filtered`: 1:1 `filtered="false"`, with template replacement `filtered="true"`
- packaged="true": all files in this `fileSet` are moved (while project creation) in a Subpackge/Directory that the user name by `project` variable 

That file also name the properites that the user has to fill in while creation
- with default
- and without default


Create Project from Archetype
-----------------------------

Create a new directory for the project and invoke

    mvn archetype:generate -DarchetypeCatalog=local



Maven Archetype Plugin
----------------------
https://maven.apache.org/archetype/maven-archetype-plugin/index.html

> The Archetype Plugin allows the user to create a Maven project from an existing template called an archetype.
>
> **It also allows the user to create an archetype from an existing project.**

### Create Archetype form Project

Creating an archetype from an existing project involves three steps:
- the archetype resolution
- the archetype installation:deployment
- the archetype usage

To create an archetype:

    mvn archetype:create-from-project

   
generates the directory tree for the archetype in `target/generated-sources/archetype` directory.  

Then move to that generated directory (`cd target/generated-sources/archetype/`) and call 

    mvn install

on the created archetype (by invoking `archetype:update-local-catalog` (that is bound to `install`)).

#### Configuration and Post generation script

The Maven Archetype Plugin has two different types of customization:
- Configuration via property file (`archetype.properties`)
- Post-Generation-Script

The configuration via properties file (`archetype.properties`) instruct the `archetype:create-from-project` goal to customize the generated archetype.
In contrast: The Post-Generation-Script (`src/main/resources/META-INF/archetype-post-generate.groovy`) is executed after the project is created from the archetype.

##### Configuration via property file (`archetype.properties`)

The create-from-project goal enable the user to provide some (sensible) configuration using the system properties or a property file.
_If property file contains sensible information then it is located outside the generated-from-project, for example in the folder above: `archetype.properties`_

   mvn archetype:create-from-project -Darchetype.properties=archetype.properties

https://maven.apache.org/archetype/maven-archetype-plugin/examples/create-with-property-file.html

The example content of the property file:
  
```
archetype.groupId=de.humanfork.experiment.archetype
archetype.artifactId=archetype-experiment
archetype.version=0.0.1-SNAPSHOT
    
# Java package of the source files; will be replaced with the ${package} expression
package=de.humanfork.experiment.template
    
#use defaults: https://maven.apache.org/archetype/maven-archetype-plugin/examples/create-with-property-file.html
#archetype.filteredExtensions=java
#archetype.languages=java

excludePatterns=createAndExecuteArchetype.bat, createArchetype.bat, executeArchetype.bat, .project

an_additional_property=my specific value
other_additional_property=${empty}
```

##### Archtetype GAV

The properties
- `archetype.groupId`,
- `archetype.artifactId` and
- `archetype.version`

name the GAV of the created archetype.

_If they are not specified, then the generated archetype use the same `groupId` as the project created from and the `articatId` get to postifx `-archetype`._

##### Rename Package

One of the most interesting feature of archetype is, that when an archtype is used to create an project, Maven will ask the user for an package and then put some classes in this package.

When using the archetype to create an project, Maven will ask the user for the package and store it in the variable `${package}`.
For all files hat resist in an `fileSet` (archetype descriptor) with `packaged="true"` become moved to an subfolder named after `${package}`.
    
    <fileSet filtered="true" packaged="true" encoding="UTF-8">
        <directory>src/main/java</directory>
        <includes>
             <include>**/*.java</include>
        </includes>
    </fileSet>

To rename the java package name at the begining of the file, the normal Maven template approach is used.
Therefore the package statement in the files must be for example: `package ${package};`

The `archetype:create-from-project` goal provides some function to convert a normal project into one that matches the expected stucture so that the `${package}` feature can been used.
Therefore the `archetype.properties` must have this settings:

    package=de.humanfork.experiment.template
    archetype.filteredExtensions=java
    archetype.languages=java

- `package` specify the package the become replaced.
- `archetype.filteredExtensions` name the file extensions (comma seperated) the the files the become filtered (replaced package and import statement)
- `archetype.languages` name the directories in `src\main\` and `src\test` thats files (if the belong th the `package`) are moved to root (so that `archtype:generate` move them back to the right location)

(One can omit `archetype.filteredExtensions` and `archetype.languages` because of there default value: listed at the bottom of https://maven.apache.org/archetype/maven-archetype-plugin/examples/create-with-property-file.html) 

The `archetype:create-from-project` goal is quit smart to handle subpackages of `project` as well as files outside of `project` correct.
Subpackages of `project` become moved relativ to `project`.
Classes outside of `project` stay where the are.

`archetype:create-from-project` will create two `fileSet`s for `src/main/java` in archetype descriptor, one with `packaged="true"` and one without.
Then it will assign the files in and outside of `project` to one of this `fileSet` with help of include and excude rules.


##### Exclude Files

The property `excludePatterns` contains a comma seperated list of excluded file patterns.
Excluding files mean, that `archetype:create-from-project` does not copy them to `target/generated-sources/archetype/` and therefore become not part of the archetype artifact.

It seam useful to exclude some the files that are excluded from commited to SCM too.  

##### Additional properties

The example `archetype.properties` contains two additional properties.
- `an_additional_property` and
- `other_additional_property`.

Both become a `requiredProperty` in `archetype-metadata.xml` 

    <requiredProperties>    
        <requiredProperty key="an_additional_property">
            <defaultValue>my specific value</defaultValue>
        </requiredProperty>
        <requiredProperty key="other_additional_propert">
            <defaultValue>${empty}</defaultValue>
        </requiredProperty>
    </requiredProperties>
    
In `archetype-metadata.xml` one could define properties with and without default values.
When the archetype used to create an project, Maven will ask the user for values of properties.

At first it seams that Maven never ask for required additional properties with default value, but this is not true.
Instead Maven ask for properties it in two rounds:

- In the first round it ask for properties without default value.
- Then it prints a summary "Confirm properties configuration" and ask: "Y:", if you enter "N"
- It starts a second round and ask for all properties.


The Maven `archetype:create-from-project` goal seams not able to handle additional properties without value.
For example, if the `archetype.properties` file contains a property without value `addititonal_wihtout_default=`, then the execution will fail:

    Failed to execute goal org.apache.maven.plugins:maven-archetype-plugin:3.0.1:create-from-project (default-cli) on project maven-template-experiment:
    The archetype is not configured

The workaround for this issue is to use undefined Maven Property.     
Because `${empty}` become a Maven Property when the Archetype is used to create an project, and ${empty} is not defined, Maven will ask the user for an value.

*Attention* the name of additional properties must not contain a `.` (dot).

#### Post-Generation-Script

If the user wants to customize the generated project even further, a groovy script named `archetype-post-generate.groovy` can be added in `src/main/resources/META-INF/`.
This script will end up in the generated archetype's `META-INF` folder and will be executed upon creating a project from this archetype.
This groovy script has access to the ArchetypeGenerationRequest object, as well as all the System.getProperties() and all the archetype generation properties the user has specified.

*This script is executed after the project is created from the archetype.*
Therefore the script is attached to the archtype and the `archtype:create` gole need to execute it.
Because this execution is done by the "client" it is a bit more unstable, and not full under control of the original author.
And at the moment it seams not to work with Eclipe M2 project creation: 
[Bug 514993 Maven archetype post generated script is not executed while creating from maven module wizard](https://bugs.eclipse.org/bugs/show_bug.cgi?id=514993)

### Testing

https://maven.apache.org/archetype/maven-archetype-plugin/integration-test-mojo.html

The user can provide archetype test projects by placing them in `src/it/projects` folder of their project.
Upon creating archetype from the existing project all the test projects will be executed to verify the archetype created by this project is good.


further modifications
=====================

`archetype:create-from-project` has some limitations for example:
- the automatic value to property replacement can only been used to source files, but not to prototype `pom.xml`
- is not able to modify the archetype pom (except GAV parameter)
- it is not able (without workaround) to add an property without default value to `archetype-metadata.xml`


This project use some Ant Tasks and XSLT to post process the files created by `archetype:create-from-project`.
This scripts are located in `src/archetype`.
(`src/archetype/resources` contains resources that are added to the prototype but are not used in the main project (for example an other `README.md`))
The post processing is executed by Ant and is defined in `src/archetype/postProcessBuild.xml`

On important modification is related to `.gitignore` and `.gitattributes` files.
The problem is, that the maven resource plugin ignore them by default.
But we want this files to be part of the prototype.
In order to add this files and make them later part of the generted project, two things need to be done
- the the files manually to the `archtype-resources` folder
- add `maven-resources-plugin` (Version > 3.0.1) with `addDefaultExcludes=false` to the `pluginManagement` of the archtetype `pom.xml` 