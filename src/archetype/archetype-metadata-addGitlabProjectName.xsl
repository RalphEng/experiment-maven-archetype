<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://maven.apache.org/plugins/maven-archetype-plugin/archetype-descriptor/1.0.0"
	xmlns:atd="http://maven.apache.org/plugins/maven-archetype-plugin/archetype-descriptor/1.0.0"
	exclude-result-prefixes="atd">
	
	<!-- Identity transform -->
	<xsl:template match="@* | node()">	
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>

	<!-- add required property: gitlab-project-name  -->
	<xsl:template match="/atd:archetype-descriptor/atd:requiredProperties/atd:requiredProperty[last()]">
		<xsl:copy-of select="."/>
		<requiredProperty key="gitlab-project-name"/>
  	</xsl:template>

</xsl:transform>