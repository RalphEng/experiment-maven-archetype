Documentation
=============

Steps after creation

    cd existing_folder
    git init
    git remote add origin git@gitlab.com:RalphEng/${gitlab-project-name}.git
    git add .
    git commit -m "Initial commit"
    git push -u origin master