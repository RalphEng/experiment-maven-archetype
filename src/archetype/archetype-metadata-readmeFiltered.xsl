<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://maven.apache.org/plugins/maven-archetype-plugin/archetype-descriptor/1.0.0"
	xmlns:atd="http://maven.apache.org/plugins/maven-archetype-plugin/archetype-descriptor/1.0.0"
	exclude-result-prefixes="atd">
	
	<!--
		Add the README.md to the files that are filtered on project generation
	-->
	
	<!-- Identity transform -->
	<xsl:template match="@* | node()">	
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	
	<!-- remove unfiltered README.md entry -->
	<xsl:template match="/atd:archetype-descriptor/atd:fileSets/atd:fileSet/atd:includes/atd:include[contains(.,'README.md')]">
  	</xsl:template>

	<!-- add new fileSet with README.md after the file set that contains the README.md before -->	
	<xsl:template match="/atd:archetype-descriptor/atd:fileSets/atd:fileSet[atd:includes/atd:include[contains(.,'README.md')]]">
		<xsl:copy>
        	<xsl:copy-of select="@*"/>
        	<xsl:apply-templates/>
    	</xsl:copy>
    	<fileSet filtered="true" encoding="UTF-8">
			<directory/>
			<includes>
				<include>README.md</include>
			</includes>
		</fileSet>
  	</xsl:template>
	 

</xsl:transform>