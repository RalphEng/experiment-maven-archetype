package de.humanfork.experiment.template.sub;
import de.humanfork.experiment.template.App;

/**
 * Hello world!
 *
 */
public class SubApp 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
