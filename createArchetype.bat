@echo ******************
@echo create archetype
@echo ******************

call mvn clean archetype:create-from-project -Darchetype.properties=archetype.properties -DaddDefaultExcludes=false
REM call mvn org.codehaus.mojo:xml-maven-plugin:1.0.2:transform@archtype-post-process -PmodifiyArchtetype
call mvn org.apache.maven.plugins:maven-antrun-plugin:1.8:run@archtype-pom-post-process -PpostByAnt

@echo ******************
@echo install archetype
@echo ******************

pushd target\generated-sources\archetype
call mvn install
popd
 